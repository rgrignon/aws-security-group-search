#!/bin/bash
# Author: Robert Grignon
# Date: 03-20-2014
#
## Description:
## Searches for all instances and Elastic Load Balancers that are part of the specified security group
##
## Usage: aws_securitygroup_search.sh <securitygroup name> <output>
##
## Examples:
##   aws_securitygroup_search.sh nagios
##

GROUP=$1

usage() {
  [ "$*" ] && echo "$0: $*"
  sed -n '/^##/,/^$/s/^## \{0,1\}//p' "$0"
  exit 2
} 2>/dev/null

if [ $# -eq 0 ]
then
   usage
fi

SG=$(aws ec2 describe-security-groups --filter Name=group-name,Values="$GROUP" --query 'SecurityGroups[*].GroupId[*]' --output text)

if [ $SG ] 
then
  echo ""
  echo ""
  echo "Security Group $GROUP"
  echo ""
  echo "Instances Found: ($SG)"
  echo "==================================="
  aws ec2 describe-instances --filter Name=instance.group-name,Values="$GROUP" --query 'Reservations[*].Instances[*].[InstanceId,Tags[*].Value[*]]' --output text
  echo ""
  echo "Load Balancers Found: ($SG)"
  echo "==================================="
  aws elb describe-load-balancers --query 'LoadBalancerDescriptions[*].[SecurityGroups, LoadBalancerName]' --output text | grep -A 1 $SG | grep -v "sg-"
  echo ""
  echo "Network Interfaces Found: ($SG)"
  echo "==================================="
  aws ec2 describe-network-interfaces --filters Name=group-id,Values="$SG" --query 'NetworkInterfaces[*].[NetworkInterfaceId,Status]' --output text
  echo ""
  echo "Nested Dependencies Found: ($SG)"
  echo "==================================="
  aws ec2 describe-security-groups --filters Name=ip-permission.group-id,Values="$SG" --query 'SecurityGroups[*].GroupName[*]' --output json | egrep -v "(^\[$)|(^\]$)" | awk -F"\"" '{ print $2 }'

else
  echo "$GROUP is an invalid "Group Name". Please check the name and try again"
fi

